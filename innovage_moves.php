<?php

/*
  Plugin Name: Innovage Moves
  Plugin URI: https://bitbucket.org/SinOB/innovage_moves
  Description: Wordpress plugin to pull-in step counts from the Moves Api.
  Author: Sinead O'Brien
  Author URI: https://bitbucket.org/SinOB
  Version: 0.1 - alpha
  Requires at least: WP 4.1
  Tested up to: WP 4.1
  License: GNU General Public License 2.0 (GPL) http://www.gnu.org/licenses/gpl.html
 */

/** /
 * Functions to add 'Moves' tab to user profile - page allows user to
 *      enable import of walking steps from Moves app. Uses OAuth. If access
 *      is approved by user we will attempt to import all user steps.
 *
 * Functions to allow iStep to communicate with moves app. After initial import
 *      iStep will import additioanl steps each time the user logs into the
 *      iStep site.
 */
// Include modified version of api wrapper by Graeme Dyas aka zabouth
include_once('PHPMoves-master/Moves.php');

function innovage_moves_init() {
    require( dirname(__FILE__) . '/innovage_moves_profile.php' );
}

add_action('bp_include', 'innovage_moves_init');

/**
 * Builds the Moves settings menus
 * @since 0.1.0
 */
function innovage_moves_admin_menu() {
    add_options_page('Moves', 'Moves for iStep', 'manage_options', 'moves', 'innovage_moves_page_settings');
}

add_action('admin_menu', 'innovage_moves_admin_menu');

/**
 * Displays the 'Home' page in settings
 * @since 0.1.0
 */
function innovage_moves_page_settings() {
    include_once( 'pages/settings.php' );
}

/**
 * Saves the settings from the options pages for Moves
 * @since 0.1.0
 */
function innovage_moves_page_save_settings() {

    if (isset($_GET['action']) && ( $_GET['action'] == 'update' )) {

        if ($_GET['page'] == 'moves') {

            update_option('moves_client_id', $_GET['movesClientId']);
            update_option('moves_client_secret', $_GET['movesClientSecret']);
            update_option('moves_redirect_url', $_GET['movesRedirectUrl']);
        }

        // Redirect back to settings page after processing
        $goback = add_query_arg('settings-updated', 'true', wp_get_referer());
        wp_redirect($goback);
    }
}

add_action('init', 'innovage_moves_page_save_settings');

/** /
 * Construct a basic request ready to be made to the Moves app
 * @return \PHPMoves\Moves
 */
function innovage_moves_request() {
    $movesClientId = get_option('moves_client_id', '');
    $movesClientSecret = get_option('moves_client_secret', '');
    $movesRedirectUrl = get_option('moves_redirect_url', '');
    $m = new PHPMoves\Moves($movesClientId, $movesClientSecret, $movesRedirectUrl);
    return $m;
}

/** /
 * Generate a request url to direct user to when requesting initial
 * authorisation for iStep to access Moves
 * @return type
 */
function innovage_moves_get_request_url() {
    $m = innovage_moves_request();
    $request_url = $m->requestURL();
    return $request_url;
}

/**
 * Convert returned authorisation code to access code and store on user account
 * @param type $authorisation_code/
 */
function innovage_moves_set_access_and_refresh_code($authorisation_code) {
    $m = innovage_moves_request();
    $authorisation_code = sanitize_text_field($authorisation_code);
    $response = $m->auth($authorisation_code);
    $access_token = $response['access_token'];
    $refresh_code = $response['refresh_token'];

    if (isset($access_token) && $access_token != '') {
        $valid = $m->validate_token($access_token);
        if ($valid) {
            $user_id = get_current_user_id();
            innovage_moves_set_access_code($user_id, $access_token);
            innovage_moves_set_refresh_code($user_id, $refresh_code);
        }
    }
}

function innovage_moves_refresh() {
    $m = innovage_moves_request();
    $user_id = get_current_user_id();
    $refresh_code = innovage_moves_get_refresh_code($user_id);
    $response = $m->refresh($refresh_code);

    if (isset($response) && !isset($response['error'])) {
        $access_token = $response['access_token'];
        $new_refresh_code = $response['refresh_token'];

        if (isset($access_token) && $access_token != '') {
            $valid = $m->validate_token($access_token);
            if ($valid) {
                $user_id = get_current_user_id();
                innovage_moves_set_access_code($user_id, $access_token);
                innovage_moves_set_refresh_code($user_id, $new_refresh_code);
            }
        }
        return $access_token;
    }
}

/** /
 * Need to keep track of users last login in order to start updating their
 * steps from the app
 * @global type $user_ID
 * @param type $login
 */
function innovage_moves_set_last_login($user_id) {
    $now = (new DateTime())->format('Ymd');
    update_user_meta($user_id, 'last_login', $now);
}

/** /
 * Return the users last login date
 * @param type $user_id
 */
function innovage_moves_get_last_login($user_id) {
    $last_login = get_user_meta($user_id, 'last_login', true);
    return $last_login;
}

/** /
 * Set moves access code
 * @param type $user_id
 * @param type $code
 */
function innovage_moves_set_access_code($user_id, $code) {
    add_user_meta($user_id, 'moves_access_code', $code);
}

/** /
 * Get moves access code
 * @param type $user_id
 * @return type
 */
function innovage_moves_get_access_code($user_id) {
    return get_user_meta($user_id, 'moves_access_code', true);
}

/** /
 * Set moves refresh code
 * @param type $user_id
 * @param type $code
 */
function innovage_moves_set_refresh_code($user_id, $code) {
    add_user_meta($user_id, 'moves_refresh_code', $code);
}

/** /
 * Get moves refresh code
 * @param type $user_id
 * @return type
 */
function innovage_moves_get_refresh_code($user_id) {
    return get_user_meta($user_id, 'moves_refresh_code', true);
}

/** /
 * If user has access code for moves import steps on login
 * @global type $user_ID
 * @param type $login
 * @return type
 */
function innovage_moves_import_since_last_login($login) {
    global $user_ID;
    $user = get_user_by('login', $login);

    // if user has moves access code get steps from move
    $access_code = innovage_moves_get_access_code($user->ID);
    if (!isset($access_code) || $access_code == '') {
        return;
    }

    $start_date = innovage_moves_get_last_login($user->ID);
    $end_date = (new DateTime())->format('Ymd');

    // import steps for each day from Moves app
    innovage_moves_import_user_steps($user->ID, $start_date, $end_date);

    // update last login
    innovage_moves_set_last_login($user->ID);
}

add_action('wp_login', 'innovage_moves_import_since_last_login');

function innovage_moves_import_user_steps($user_id, $start, $end) {

    $access_code = innovage_moves_get_access_code($user_id);
    $endpoint = '/user/summary/daily';
    $m = innovage_moves_request();
    $data = $m->get_range($access_code, $endpoint, $start, $end);

    //if we received an expired access token response from request
    if ($data == 'expired_access_token') {
        // attempt to refresh access token
        $new_access_code = innovage_moves_refresh();
        $data = $m->get_range($new_access_code, $endpoint, $start, $end);
    }

    // failed to refresh - probably user as revoked access
    if ($data == 'expired_access_token') {
        return;
    }
    echo '<h2>Importing the following data from Moves app</h2>';
    echo '<table></tr><th>Date</th><th>Steps Walked</th><tr>';
    foreach ($data as $day) {
        if (isset($day['summary'])) {
            foreach ($day['summary'] as $activity) {
                if ($activity['activity'] == 'walking') {
                    echo '<tr><td>'.$day['date'].'</td><td>'.$activity['steps'].'</td></tr>';

                    $date = new DateTime($day['date']);
                    $step_count = $activity['steps'];

                    if ( function_exists('innovage_pedometer_update_steps')) {
                        // save steps to step table
                        innovage_pedometer_update_steps($user_id, $date, $step_count);
                    }
                }
            }
        }
    }
    echo '</table>';

    return $data;
}

function innovage_moves_get_current_user_profile() {
    $user_id = get_current_user_id();
    $access_code = innovage_moves_get_access_code($user_id);
    $m = innovage_moves_request();
    $data = $m->get_profile($access_code);
    if ($data == 'expired_access_token') {
        // attempt to refresh access token
        $new_access_code = innovage_moves_refresh();
        // retry attempt to get profile
        $data = $m->get_profile($new_access_code);
    }
    return $data;
}

function innovage_moves_import_initial() {

    $profile = innovage_moves_get_current_user_profile();
    if (!isset($profile) || !isset($profile['profile'])) {
        echo '<br/>Unable to perform initial import of Moves step data. Please refresh your key to try again.';
        return;
    }

    $user_id = get_current_user_id();

    // Start date to import data from moves
    $registered_istep_date = new DateTime(get_userdata($user_id)->user_registered);
    $registered_moves_date = new DateTime($profile['profile']['firstDate']);

    $start = $registered_moves_date;
    if ($registered_istep_date > $registered_moves_date) {
        $start = $registered_istep_date;
    }

    // End date is  todays date
    $end = new DateTime();

    $interval = new DateInterval('P1M');
    $daterange = new DatePeriod($start, $interval, $end);

    $i = 0;
    // Can only import one month of data at a time so for each month
    foreach ($daterange as $date) {
        $i++;
        if ($i === 1) {
            // If start of range use first day of membership
            // moves will not allow query date before user was a member
            $s = $start->format("Ymd");
        } else {
            // otherwise use first day of month for every following month
            $s = $date->format("Ym01");
        }
        if (count($daterange) === $i) {
            // if last month is current month use today as last date
            $e = $end->format("Ymd");
        } else {
            // otherwise use last day of month
            $e = $date->modify('last day of this month')->format("Ymd");
        }

        innovage_moves_import_user_steps($user_id, $s, $e);
    }
}

/** /
 * Revoke a users approval to allow iStep to import walking step data
 * from Moves app.
 * @param type $user_id
 */
function innovage_moves_revoke_access($user_id) {
    delete_user_meta($user_id, 'moves_access_code');
    delete_user_meta($user_id, 'moves_refresh_code');
}
