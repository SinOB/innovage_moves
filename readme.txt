=== Innovage Moves ===
Contributors: SinOB
Tags: innovage
Requires at least: WP 4.1
Tested up to: WP 4.1
Stable tag: 0.1-alpha
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Wordpress plugin to pull-in step counts from the Moves Api.

== Description ==

Allows members to link their iStep account to their Moves account via OAuth 2.0
and then imports step counts from the moves app.

Admin options to set api client id and secret code.


== Installation ==

1. Download
2. Upload to your '/wp-contents/plugins/' directory.
3. Activate the plugin through the 'Plugins' menu in WordPress.
