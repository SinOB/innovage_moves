<?php
/*
 * Handles displaying the page on member profile to allow the user to connect
 * with the MovesAPI 
 */

function innovage_moves_member_setup_nav() {
    global $bp;

    $parent_slug = 'moves';
    bp_core_new_nav_item(array(
        'name' => __('Moves'),
        'slug' => $parent_slug,
        'screen_function' => 'innovage_moves_member_screen',
        'position' => 40,
        'default_subnav_slug' => 'test_sub'));

    function innovage_moves_member_show_screen_title() {
        
    }

    function innovage_moves_member_show_screen_content() {
        $user_id = get_current_user_id();
        $url = innovage_moves_get_request_url();

        // User has requested to revoke access to moves app
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['revoke'])) {
            innovage_moves_revoke_access($user_id);
        }

        // Returned from moves app site with authorisation_code
        if ($_SERVER['REQUEST_METHOD'] === 'GET' && isset($_GET['code'])) {
            innovage_moves_set_access_and_refresh_code($_GET['code']);

            // import user walking steps from Moves since they joined Moves
            innovage_moves_import_initial();
        }
        $moves_access_code = get_user_meta($user_id, 'moves_access_code', true);

        // User already has access code
        if (isset($moves_access_code) && $moves_access_code != '') {
            ?>
            Your iStep account is currently linked with your moves app.
            Your current code is <?php echo $moves_access_code; ?>.
            <br/><br/>
            <form method='POST'>
                <input type='submit' name='revoke' value='Revoke access'>

                <a href="<?php echo $url; ?>"><input type='button' value='Reset access code'>
                    </form>
                    <?php
                    innovage_moves_import_initial();
                } else {
                    // User has not authorised Moves to work with iStep
                    ?>
                    If you are already using the Moves App on your mobile phone we can download your 
                    walking step count to iStep automatically. Click on the button below to 
                    link your accounts. This will temporarily redirect you to the Moves website.
                    You will be returned to iStep once the process is complete.
                    <br/>
                    <a href="<?php echo $url; ?>"><input type='button' value='Link accounts'>
            <?php
        }
    }

}

function innovage_moves_member_screen() {
    add_action('bp_template_title', 'innovage_moves_member_show_screen_title');
    add_action('bp_template_content', 'innovage_moves_member_show_screen_content');
    bp_core_load_template(apply_filters('bp_core_template_plugin', 'members/single/plugins'));
}

add_action('bp_setup_nav', 'innovage_moves_member_setup_nav');

