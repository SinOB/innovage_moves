<?php
/* //
 * Handles display for admin settings page for moves app
 */
$movesClientId = get_option('moves_client_id', '');
$movesClientSecret = get_option('moves_client_secret', '');
$movesRedirectUrl = get_option('moves_redirect_url', '');

?>

<div class="wrap about-wrap">
    <h1>Moves for iStep</h1>
    <h2 class="nav-tab-wrapper">
        <a href="?page=moves" class="nav-tab nav-tab-active">Settings</a>
    </h2>
    <p class="about-description">These details are available in your <a href="https://dev.moves-app.com/apps" target="_blank">Moves dashboard</a> after signup. Without them, WordPress won't be able to communicate on your behalf.</p>
    <form action="<?php echo admin_url('options-general.php?page=moves'); ?>">
        <input type="hidden" name="page" value="moves">
        <input type="hidden" name="action" value="update">
        <?php wp_nonce_field(); ?>
        <table class="form-table">
            <tbody>
                
                <tr valign="top">
                    <th scope="row"><label for="movesClientId">Client ID</label></th>
                    <td>
                        <input name="movesClientId" type="text" value="<?php echo $movesClientId; ?>" class="regular-text">
                    </td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="movesClientSecret">Client Secret</label></th>
                    <td><input name="movesClientSecret" type="text" value="<?php echo $movesClientSecret; ?>" class="regular-text"></td>
                </tr>
                <tr valign="top">
                    <th scope="row"><label for="movesRedirectUrl">Redirect Url</label></th>
                    <td><input name="movesRedirectUrl" type="text" value="<?php echo $movesRedirectUrl; ?>" class="regular-text"></td>
                </tr>
            </tbody>
        </table>
        <p class="submit">
            <input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes">
        </p>
    </form>
    <hr />
</div>
